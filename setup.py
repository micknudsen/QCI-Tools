from setuptools import setup

setup(

    test_suite='tests',

    name='QCI Tools',

    description='Python tool for easy communication with the Qiagen Clinical Insight API',

    author='Michael Knudsen',
    author_email='micknudsen@gmail.com',

    packages=['qcitools'],

    scripts=['scripts/qcitools'],

    install_requires=['requests', 'click', 'pandas']

)
