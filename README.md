# QCI-Tools
Tool to help human beings communicate with the Qiagen Clinical Insight API.

Since the API is very poorly documented, it has taken a lot of trial-and-error to get a sense of what query responses may look like. Please let me know if you manage to somehow break the tool.

# Installation

QCI-Tools is written in __Python 3.5__ and can be installed using `pip`.

```
	git clone https://gitlab.com/MOMA/QCI-Tools.git
	pip install QCI-Tools/
```

# Usage

Assuming that the installation was successfull, QCI-Tools can now be run from the command line.

```
[micknudsen@work:~]$ qcitools
Usage: qcitools [OPTIONS] COMMAND [ARGS]...

  Python tool for interacting with the Qiagen Clinical Insight API.

Options:
  --help  Show this message and exit.

Commands:
  access_token
  add_user
  export_results
  get_status
  submit_test
  variants_to_table
```

Note that QCI-Tools requires your _client ID_ and _client secret_ to be set as environment variables.
```
export QCI_CLIENT_ID="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
export QCI_CLIENT_SECRET="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

# Tools

## access_token

The `access_token` tool requests an access token from QCI and prints it in the console. This can be handy if you want to make custom API calls that are not supported by QCI-Tools.

```
[micknudsen@work:~]$ qcitools access_token
XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
```

## submit_test

The `submit_test` tool uploads a test to QCI. The input file must be a zip archive containing variants (VCF format) and patient metadata (XML format).

```
[micknudsen@work:~]$ qcitools submit_test my_test.zip
[INFO] Submitted test with package_id DP_XXXXXXXXXXXXXXXXXXXXXX
[INFO] Processing...
[INFO] Processing...
[INFO] Done! View results in QCI at https://apps.qiagenbioinformatics.eu/vcs/view/analysis/XXXXXXX
```

## get_status

The `get_status` tool print the status of a submitted test.

```
[micknudsen@work:~]$ qcitools get_status DP_XXXXXXXXXXXXXXXXXXXXXX
DONE
```

## add_user

The `add_user` tool adds a user (email address) to an existing test. The user is required to have a QCI/VA account.

```
[micknudsen@work:~]$ qcitools add_user --package_id DP_XXXXXXXXXXXXXXXXXXXXXX --email_address foo@example.com
[ERROR] Sharing cannot be completed because one or more users foo@example.com do not have a (QCI | VA) account.
```

## export_results

The `export_results` tool export results as either VCF, XML, or PDF. This tool should be run after the analysis on the QCI webpage has been completed.

```
[micknudsen@work:~]$ qcitools export_results --package_id DP_XXXXXXXXXXXXXXXXXXXXXX --output_format pdf --output_file report.pdf
[INFO] Done!
```

## variants_to_table

The `variants_to_table` tool takes as input a VCF file exported from QCI using the `export_results` tool. The output is a table (tab separated values) containing the most important information about the variants. By default, all splice variants are included. To include only one transcript for each variant, add the `--simple_output` flag.

```
[micknudsen@work:~]$ qcitools variants_to_table --vcf_file foo.vcf --output_file bar.tsv
[INFO] Done!
```
