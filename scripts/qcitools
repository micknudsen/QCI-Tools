#!/usr/bin/env python3.5

import os
import sys
import click
import logging

from qcitools import QCIClient
from qcitools.parsers import make_table

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

client_id = os.environ.get('QCI_CLIENT_ID')
client_secret = os.environ.get('QCI_CLIENT_SECRET')

if not client_id:
    logging.error('Environment variable QCI_CLIENT_ID is not set')
    sys.exit(1)

if not client_secret:
    logging.error('Environment variable QCI_CLIENT_SECRET is not set')
    sys.exit(1)

qci_client = QCIClient(client_id=client_id, client_secret=client_secret)

@click.group()
def run():
    '''Python tool for interacting with the Qiagen Clinical Insight API.'''
    pass

@click.command()
def access_token():
    print(qci_client.access_token)

@click.command()
@click.argument('input_file', required=True)
def submit_test(input_file):
    qci_client.submit_test(input_file=input_file)

@click.command()
@click.argument('package_id', required=True)
def get_status(package_id):
    print(qci_client.get_status(package_id=package_id)['status'])


@click.command()
@click.option('--package_id', required=True)
@click.option('--email_address', required=True)
def add_user(package_id, email_address):
    print(qci_client.add_user(package_id=package_id, email_address=email_address))

@click.command()
@click.option('--package_id', required=True)
@click.option('--output_format', required=True, type=click.Choice(['vcf', 'xml', 'pdf']))
@click.option('--output_file', required=True)
def export_results(package_id, output_format, output_file):
    qci_client.export_results(package_id=package_id, output_format=output_format, output_file=output_file)

@click.command()
@click.option('--vcf_file', required=True)
@click.option('--output_file', required=True)
@click.option('--simple_output', required=False, is_flag=True)
def variants_to_table(vcf_file, simple_output, output_file):
    make_table(vcf_file=vcf_file, simple_output=simple_output, output_file=output_file)


run.add_command(access_token)
run.add_command(submit_test)
run.add_command(get_status)
run.add_command(add_user)
run.add_command(export_results)
run.add_command(variants_to_table)

if __name__ == '__main__':
    run()
