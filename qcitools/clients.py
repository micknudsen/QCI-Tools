import sys
import time
import logging
import requests

ACCESS_TOKEN_URL = 'https://api.qiagenbioinformatics.eu/v1/oauth/access_token'
STATUS_URL = 'https://api.qiagenbioinformatics.eu/v1/datapackages/{package_id}'
ADD_USER_URL = 'https://api.qiagenbioinformatics.eu/datastream/api/v1/datapackages/{package_id}/users'
EXPORT_URL = 'https://api.qiagenbioinformatics.eu/v1/export/{package_id}'
SUBMIT_TEST_URL = 'https://api.qiagenbioinformatics.eu/datastream/api/v1/datapackages/'


class QCIClient:

    def __init__(self, client_id, client_secret):
        self._client_id = client_id
        self._client_secret = client_secret
        self._access_token = None

    def _make_get_request(self, *args, **kwargs):
        response = requests.get(*args, **kwargs, headers={'Authorization': self.access_token})
        try:
            # In some cases, there is no JSON representation of the response. For
            #  example, this is the case for the response from export_results.
            if response.json().get('error') == 'invalid access token':
                self.access_token = self._request_access_token()
                return self._make_get_request(*args, **kwargs)
        except Exception:
            pass
        return response

    def _make_post_request(self, *args, **kwargs):
        response = requests.post(*args, **kwargs, headers={'Authorization': self.access_token})
        if response.json().get('error') == 'invalid access token':
            self.access_token = self._request_access_token()
            return self._make_post_request(*args, **kwargs)
        return response

    def _request_access_token(self):
        '''Request an access token from the Qiagen server. Note
           that access tokens expire after five minutes.'''

        response = requests.get(ACCESS_TOKEN_URL, params={
                                'grant_type': 'client_credentials',
                                'client_id': self._client_id,
                                'client_secret': self._client_secret}).json()

        try:
            # If client_id and client_secret are valid, there is
            # an 'access_token' key in the JSON object.
            return response['access_token']
        except KeyError:
            # Something went wrong, so there should be an 'error' key
            # in the JSON object containing the error message.
            logging.error(response['error'])
            sys.exit(1)

    @property
    def access_token(self):
        if not self._access_token:
            self._access_token = self._request_access_token()
        return self._access_token

    @access_token.setter
    def access_token(self, new_access_token):
        self._access_token = new_access_token

    def get_status(self, package_id):
        '''Check status of submittet test.'''

        response = self._make_get_request(STATUS_URL.format(package_id=package_id)).json()

        def log_errors(errors):
            for error in errors:
                logging.error(error.get('message'))

        try:
            if response['status'] == 'FAILED':
                log_errors(response['errors'])
            return response
        except KeyError:
            log_errors(response['errors'])
            sys.exit(1)

    def add_user(self, package_id, email_address):
        '''Add a user (email address) to an existing test.'''

        response = self._make_post_request(ADD_USER_URL.format(package_id=package_id), data='[{"email": "%s"}]' % email_address).json()

        # If operation succeeded, the response is a list of the email
        # addresses with access to the analysis.
        if isinstance(response, list):
            return response

        # Something went wrong...
        logging.error(response['error'])
        sys.exit(1)

    def export_results(self, package_id, output_format, output_file):
        '''Export test results as xml, vcf, or pdf.'''

        export_url = '{export_url}?view={output_format}'.format(export_url=EXPORT_URL.format(package_id=package_id), output_format=output_format)
        response = self._make_get_request(export_url)

        with open(output_file, 'wb') as f:
            f.write(response.content)

        logging.info('Done!')

    def submit_test(self, input_file):
        '''Submit a zip file containing variants and metadata.'''

        with open(input_file, 'rb') as f:
            response = self._make_post_request(SUBMIT_TEST_URL, files={'file': f}).json()

        # There is no field specifically for the package ID, but we can extract
        # if from the 'status-url' entry. Assertion just in case...
        package_id = response['status-url'].split('/')[-1]

        logging.info('Submitted test with package_id %s', package_id)

        # Check status every ten seconds until status if DONE or FAILED.
        while response['status'] in ['PREPROCESSING', 'PROCESSING']:
            logging.info('Processing...')
            time.sleep(60)
            response = self.get_status(package_id)

        if response['status'] == 'FAILED':
            # Many things can go wrong. Print all errors.
            for error in response['errors']:
                logging.error(error['message'])
            sys.exit(1)

        # If we made it this far, everything should be OK!
        assert response['status'] == 'DONE'
        logging.info('Done! View results in QCI at %s', response['results-redirect-url'])
