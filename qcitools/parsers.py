import logging
import pandas as pd


def make_table(vcf_file, simple_output, output_file):

    # Parse the VCF file exported from QCI.
    vcf_table = _read_vcf_file(vcf_file)

    result = pd.DataFrame()

    # Grab some columns from the VCF file.
    result['Chromosome'] = vcf_table['#CHROM'].apply(lambda x: x.replace('chr', ''))
    result['Position'] = vcf_table['POS']
    result['Allele Fraction'] = vcf_table.apply(lambda x: _format_field('ING_AF', x), axis=1)
    result['Read Depth'] = vcf_table.apply(lambda x: _format_field('DP', x), axis=1)
    result['Effect'] = vcf_table.apply(lambda x: _format_field('ING_IA', x), axis=1)

    # Add extra colums based on information from INFO fields.
    selected_info_fields = {
        'Reported': 'CLI_REPORTABLE',
        'Gene': 'GENE_SYMBOL',
        'Region': 'GENE_REGION',
        'Transcript': 'TRANSCRIPT_ID',
        'Nucleotide': 'HGVS_TRANSCRIPT',
        'Protein': 'HGVS_PROTEIN',
        'Impact': 'TRANSLATION_IMPACT',
        'Classification': 'ING_CLASSIFICATION'
    }

    for name in selected_info_fields:
        result[name] = vcf_table.apply(lambda row, name=name: _info_field(selected_info_fields[name], row), axis=1)

    # Indicate wheter variant has been selected for the report in QCI.
    result['Reported'] = result['Reported'].apply(lambda x: 'Yes' if x == 'Reportable' else '')

    # Remove variants not associated with genes.
    result = result[result['Gene'] != '']

    # Strip quotes from Classification column.
    result['Classification'] = result['Classification'].apply(lambda x: x.strip('"'))

    # Remove duplicates (corresponding to different transcripts).
    if simple_output:
        selected_row_indices = [group.index[0] for _, group in result.groupby(['Chromosome', 'Position'])]
        result = result.loc[selected_row_indices, :]

    # Remove encapsulating quotes
    result['Impact'] = result['Impact'].apply(lambda x: x.strip('"'))

    # Everthing looks better capitalized.
    result['Impact'] = result['Impact'].apply(str.title)
    result['Effect'] = result['Effect'].apply(str.title)

    # Define the order of chromosomes. Always a mess since the names can
    # be both integers and letters...
    chromosome_order = ['M'] + [str(n) for n in range(1, 23)] + ['X', 'Y']
    result['Chromosome'] = pd.Categorical(result['Chromosome'], chromosome_order)

    # Sort dataframe by position in genome.
    result = result.sort_values(['Chromosome', 'Position'])

    # Get the right column ordering.
    result = result[['Reported', 'Chromosome', 'Position', 'Gene', 'Region', 'Transcript', 'Nucleotide',
                     'Protein', 'Allele Fraction', 'Read Depth', 'Impact', 'Classification', 'Effect']]

    result.to_csv(output_file, sep='\t', index=False)
    logging.info('Done!')


def _read_vcf_file(vcf_file):
    '''Returns contents of VCF file as a pandas dataframe'''

    # Since the comment argument in pandas.read_csv only accepts strings
    # of length one, we have to manually locate the line cotaining column
    # names. It starts with # while header lines start with ##.
    with open(vcf_file, 'r') as f:
        for line in f:
            if not line.startswith('##'):
                vcf_columns = line.strip().split('\t')
                break

    # Read VCF file skipping both header lines and column names. Manually
    # add column names extracted above.
    return pd.read_csv(vcf_file, sep='\t', comment='#', names=vcf_columns)


def _info_field(name, row):
    '''Return data corresponding to 'name' in INFO filed in 'row' of VCF file.
       If 'name' is not found, an empty string is returned.'''

    # A general info field is on the form a1=b1;a2=b2;...;an=bn. Since some
    # of the b's may contain semicolons inside quotes, e.g.
    #
    # CLINVAR="RCV000150897.1; RCV000144970.3; OMIM_gene:190070.0005; RCV000013412.5;"
    #
    # we cannot simply split the string on semicolons. We therefore read the string
    # one character at the time and ignore semicolons whenever we are inside quotes.

    row_info = row['INFO']

    token = ''
    ignore_delimiter = False

    for character in row_info:

        # When a quote is encountered, we flip whether semicolons
        # should be ignored as delimiters or not.
        if character == '"':
            ignore_delimiter = not ignore_delimiter

        # If the semicolon is ignored, it must be added to the token.
        if ignore_delimiter:
            token += character

        else:
            # If the semicolon is regarded as a delimiter, the token is
            # complete. It the token has the right name, return its value.
            if character == ';':
                split_token = token.split('=')
                if split_token[0] == name:
                    return split_token[1]

                token = ''
            # If the character is not a semicolon, add it to the token.
            else:
                token += character

    # If the INFO string does not end with a semicolon (which it probably
    # never does, make sure to check the last token.
    if token != '':
        split_token = token.split('=')
        if split_token[0] == name:
            return split_token[1]

    return ''


def _format_field(field, row):
    '''Returns value of format field correspondig to variant in VCF file'''

    # Standard format of a VCF file.
    format_column_index = row.index.values.tolist().index('FORMAT')
    values_column_index = format_column_index + 1

    # Find out which entry in the format string corresponds to the allele
    # fraction and return the corresponding entry from the values string.
    field_index = row[format_column_index].split(':').index(field)
    return row[values_column_index].split(':')[field_index]
